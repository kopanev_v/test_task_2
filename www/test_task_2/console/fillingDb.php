<?php

require_once __DIR__ . '/../vendor/autoload.php';

use common\models\Db;

$db = Db::getInstance();

$names  = ['Mike', 'Tony', 'Ivan', 'John', 'Jack', 'Walter', 'Jesse'];

$countRows = 5000;

for($i=0; $i < $countRows; $i++) {
    $name = $names[rand(0, count($names)-1)];

    $data = $db->prepare('INSERT INTO author SET name = :name');
    $data->execute([':name' => $name]);
}

for($i=0; $i < $countRows; $i++) {
    $title = 'title news '.date('Y-m-d', time() - rand(0, 99999999));
    $authorId = rand(1, $countRows);

    $data = $db->prepare('INSERT INTO news SET author_id = :author_id, title = :title');
    $data->execute([':author_id' => $authorId, ':title' => $title]);
}


$catalog = $db->prepare('SELECT id FROM `catalog`');
$catalog->execute();
$catalog = $catalog->fetchAll(\PDO::FETCH_OBJ);

for($i=0; $i < ($countRows * 3); $i++) {
    $catalogId = $catalog[rand(0, count($catalog)-1)]->id;
    $newsId = rand(1, $countRows);

    $data = $db->prepare('INSERT INTO catalog_news SET catalog_id = :catalog_id, news_id = :news_id');
    $data->execute([':catalog_id' => $catalogId, ':news_id' => $newsId]);
}
