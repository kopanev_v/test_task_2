<?php

namespace common\models;

class Db
{
    /**
     * @var null
     */
    protected static $instance = null;

    public static function getInstance() {
        if (self::$instance === null) {
            try {
                self::$instance = new \PDO(
                    'mysql:host='.CONFIG_DBHOST.';dbname='.CONFIG_DBNAME,
                    CONFIG_DBUSER,
                    CONFIG_DBPASS
                );
            }
            catch (\PDOException $e) {
                throw new \Exception ($e->getMessage());
            }
        }

        return self::$instance;
    }
}