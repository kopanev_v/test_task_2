<?php


namespace common\models;


class CatalogRepository
{
    protected static $table = 'catalog';
    protected static $tableCatalogNews = 'catalog_news';

    private $db;

    public function __construct() {
        $this->db = Db::getInstance();
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function getById($id) {
        $data = $this->db->prepare('SELECT * FROM `'.self::$table.'` WHERE `id` = :id');
        $data->bindValue(':id', $id, \PDO::PARAM_INT);
        $data->execute();
        return $data->fetch(\PDO::FETCH_OBJ);
    }
}