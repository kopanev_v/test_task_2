<?php


namespace common\models;


class NewsRepository
{
    protected static $table = 'news';
    protected static $tableCatalog = 'catalog';
    protected static $tableCatalogNews = 'catalog_news';

    private $db;

    public function __construct() {
        $this->db = Db::getInstance();
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function getById($id) {
        $data = $this->db->prepare('SELECT * FROM `'.self::$table.'` WHERE `id` = :id');
        $data->bindValue(':id', $id, \PDO::PARAM_INT);
        $data->execute();
        return $data->fetch(\PDO::FETCH_OBJ);
    }

    /**
     * @param int $authorId
     * @param int $offset
     * @param int $limit
     * @return array
     */
    public function getByAuthor($authorId, $offset, $limit) {
        $data = $this->db->prepare('SELECT * FROM `'.self::$table.'` 
            WHERE `author_id` = :author_id 
            LIMIT :limit OFFSET :offset');
        $data->bindValue(':author_id', $authorId, \PDO::PARAM_INT);
        $data->bindValue(':limit', $limit, \PDO::PARAM_INT);
        $data->bindValue(':offset', $offset, \PDO::PARAM_INT);
        $data->execute();

        return $data->fetchAll(\PDO::FETCH_OBJ);
    }

    /**
     * @param int $catalogId
     * @param int $offset
     * @param int $limit
     * @return array
     */
    public function getByCatalogId($catalogId, $offset, $limit) {
        $data = $this->db->prepare('SELECT n.* 
            FROM `'.self::$tableCatalogNews.'` cn 
            LEFT JOIN `'.self::$table.'` n ON cn.news_id = n.id    
            WHERE cn.catalog_id = :catalog_id 
            LIMIT :limit OFFSET :offset');
        $data->bindValue(':catalog_id', $catalogId, \PDO::PARAM_INT);
        $data->bindValue(':limit', $limit, \PDO::PARAM_INT);
        $data->bindValue(':offset', $offset, \PDO::PARAM_INT);
        $data->execute();

        return $data->fetchAll(\PDO::FETCH_OBJ);
    }

    /**
     * @param string $title
     * @param int $offset
     * @param int $limit
     * @return array
     */
    public function findByTitle($title, $offset, $limit) {
        $data = $this->db->prepare('SELECT * FROM `'.self::$table.'` 
            WHERE `title` LIKE :title 
            LIMIT :limit OFFSET :offset');
        $data->bindValue(':title', '%'.$title.'%');
        $data->bindValue(':limit', $limit, \PDO::PARAM_INT);
        $data->bindValue(':offset', $offset, \PDO::PARAM_INT);
        $data->execute();

        return $data->fetchAll(\PDO::FETCH_OBJ);
    }

    /**
     * @param string $title
     * @param int $catalogId
     * @param int $offset
     * @param int $limit
     * @return array
     */
    public function findByTitleCatalogOne($title, $catalogId, $offset, $limit) {
        $data = $this->db->prepare('SELECT n.* 
            FROM `'.self::$tableCatalogNews.'` cn 
            LEFT JOIN `'.self::$table.'` n ON cn.news_id = n.id    
            WHERE cn.catalog_id = :catalog_id AND `title` LIKE :title 
            LIMIT :limit OFFSET :offset');
        $data->bindValue(':title', '%'.$title.'%');
        $data->bindValue(':catalog_id', $catalogId, \PDO::PARAM_INT);
        $data->bindValue(':limit', $limit, \PDO::PARAM_INT);
        $data->bindValue(':offset', $offset, \PDO::PARAM_INT);
        $data->execute();

        return $data->fetchAll(\PDO::FETCH_OBJ);
    }


    /**
     * @param string $title
     * @param object $catalog
     * @param int $offset
     * @param int $limit
     * @return array
     */
    public function findByTitleCatalogIncludeChildren($title, $catalog, $offset, $limit) {
        $data = $this->db->prepare('SELECT n.* 
            FROM `'.self::$tableCatalog.'` c 
            LEFT JOIN `'.self::$tableCatalogNews.'` cn ON cn.catalog_id = c.id 
            LEFT JOIN `'.self::$table.'` n ON cn.news_id = n.id 
            WHERE 
                c.lft >= :lft AND c.rgt <= :rgt 
                AND                 
                `title` LIKE :title 
            GROUP BY n.id
            LIMIT :limit OFFSET :offset');
        $data->bindValue(':title', '%'.$title.'%');
        $data->bindValue(':lft', $catalog->lft, \PDO::PARAM_INT);
        $data->bindValue(':rgt', $catalog->rgt, \PDO::PARAM_INT);
        $data->bindValue(':limit', $limit, \PDO::PARAM_INT);
        $data->bindValue(':offset', $offset, \PDO::PARAM_INT);
        $data->execute();

        return $data->fetchAll(\PDO::FETCH_OBJ);
    }
}