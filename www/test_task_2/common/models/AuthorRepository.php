<?php


namespace common\models;


class AuthorRepository
{
    protected static $table = 'author';

    private $db;

    public function __construct() {
        $this->db = Db::getInstance();
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function getById($id) {
        $data = $this->db->prepare('SELECT * FROM `'.self::$table.'` WHERE `id` = :id');
        $data->bindValue(':id', $id, \PDO::PARAM_INT);
        $data->execute();
        return $data->fetch(\PDO::FETCH_OBJ);
    }

    /**
     * @param int $offset
     * @param int $limit
     * @return array
     */
    public function getAll($offset, $limit) {
        $data = $this->db->prepare('SELECT * FROM `'.self::$table.'` LIMIT :limit OFFSET :offset');
        $data->bindValue(':limit', $limit, \PDO::PARAM_INT);
        $data->bindValue(':offset', $offset, \PDO::PARAM_INT);
        $data->execute();
        return $data->fetchAll(\PDO::FETCH_OBJ);
    }
}