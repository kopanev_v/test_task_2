<?php

require_once __DIR__ . '/../vendor/autoload.php';

use common\models\AuthorRepository;

$response = ['result' => 'empty result'];
if(empty($_REQUEST['action'])) {
    $response['error'][] = 'empty action';
}
else {
    $aRep = new AuthorRepository();
    $result = [];
    $limit = 100;
    $offset = 0;

    switch ($_REQUEST['action']) {
        case 'getAll' : {
            while($items = $aRep->getAll($offset, $limit)) {
                $result = array_merge($result, $items);
                $offset += $limit;
            }

            break;
        }

        default : {
            $response['error'][] = 'incorrect action';
        }
    }


    if($result) {
        $response['result'] = $result;
    }
}

header('Content-Type: application/json');
echo json_encode($response);