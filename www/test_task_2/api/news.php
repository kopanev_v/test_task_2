<?php

require_once __DIR__ . '/../vendor/autoload.php';

use common\models\NewsRepository;
use common\models\CatalogRepository;

$response = ['result' => 'empty result'];
if(empty($_REQUEST['action'])) {
    $response['error'][] = 'empty action';
}
else {
    $nRep = new NewsRepository();
    $result = [];
    $limit = 100;
    $offset = 0;

    switch ($_REQUEST['action']) {
        case 'getByAuthorId' : {
            if(empty($_REQUEST['author_id'])) {
                $response['error'][] = 'empty author_id';
            }
            else {
                $authorId = $_REQUEST['author_id'];

                while($items = $nRep->getByAuthor($authorId, $offset, $limit)) {
                    $result = array_merge($result, $items);
                    $offset += $limit;
                }
            }

            break;
        }

        case 'getByCatalogId' : {
            if(empty($_REQUEST['catalog_id'])) {
                $response['error'][] = 'empty catalog_id';
            }
            else {
                $catalogId = $_REQUEST['catalog_id'];

                while($items = $nRep->getByCatalogId($catalogId, $offset, $limit)) {
                    $result = array_merge($result, $items);
                    $offset += $limit;
                }
            }

            break;
        }

        case 'getByIds' : {
            if(empty($_REQUEST['ids'])) {
                $response['error'][] = 'empty ids';
            }
            else {
                $ids = unserialize($_REQUEST['ids']);

                foreach ($ids as $id) {
                    $item = $nRep->getById($id);
                    if($item) {
                        $result[] = $item;
                    }
                }
            }

            break;
        }

        case 'findByTitle' : {
            if(empty($_REQUEST['find'])) {
                $response['error'][] = 'empty find';
            }
            else {
                $find = $_REQUEST['find'];

                while($items = $nRep->findByTitle($find, $offset, $limit)) {
                    $result = array_merge($result, $items);
                    $offset += $limit;
                }
            }

            break;
        }


        case 'findByTitleCatalogOne' : {
            if(empty($_REQUEST['find'])) {
                $response['error'][] = 'empty find';
            }

            if(empty($_REQUEST['catalog_id'])) {
                $response['error'][] = 'empty catalog_id';
            }

            if(empty($response['error'])) {
                $find      = $_REQUEST['find'];
                $catalogId = $_REQUEST['catalog_id'];

                while($items = $nRep->findByTitleCatalogOne($find, $catalogId, $offset, $limit)) {
                    $result = array_merge($result, $items);
                    $offset += $limit;
                }
            }

            break;
        }


        case 'findByTitleCatalogIncludeChildren' : {
            if(empty($_REQUEST['find'])) {
                $response['error'][] = 'empty find';
            }

            if(empty($_REQUEST['catalog_id'])) {
                $response['error'][] = 'empty catalog_id';
            }

            if(empty($response['error'])) {
                $find      = $_REQUEST['find'];
                $catalogId = $_REQUEST['catalog_id'];

                $catalog = (new CatalogRepository())->getById($catalogId);
                if($catalog) {
                    while($items = $nRep->findByTitleCatalogIncludeChildren($find, $catalog, $offset, $limit)) {
                        $result = array_merge($result, $items);
                        $offset += $limit;
                    }
                }
            }

            break;
        }

        default : {
            $response['error'][] = 'incorrect action';
        }
    }


    if($result) {
        $response['result'] = $result;
    }
}

header('Content-Type: application/json');
echo json_encode($response);