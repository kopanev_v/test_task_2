из консоли
---
- docker-compose up --build -d
- docker-compose run --rm php composer install
- docker-compose run --rm php php console/fillingDb.php
---




все методы возвращают результат в секции result, а ошибки в error


NEWS
---
 - getByAuthorId -- 
 возвращает новости по id автора -- 
 localhost:8000/news.php?action=getByAuthorId&author_id=5
 
 - getByCatalogId -- 
 возвращает новости по id сатегории (дочерние категории не учитываются) -- 
 localhost:8000/news.php?action=getByCatalogId&catalog_id=3
 
 - getByIds -- 
 возвращает новости по массиву id (сериализованный массив) -- 
 localhost:8000/news.php?action=getByIds&ids=a:5:{i:0;i:1;i:1;i:3;i:2;i:5;i:3;i:7;i:4;i:8;}
 
 - findByTitle -- 
 Ищет новости по названию -- 
 localhost:8000/news.php?action=findByTitle&find=2020-03
 
 - findByTitleCatalogOne -- 
 Ищет новости по названию в определенной категории(без учета дочених категорий) -- 
 localhost:8000/news.php?action=findByTitleCatalogOne&find=2017&catalog_id=3
 
 - findByTitleCatalogBranch -- 
 Ищет новости по названию в категории(c учетом дочених категорий) -- 
 localhost:8000/news.php?action=findByTitleCatalogIncludeChildren&catalog_id=1&find=2020-03
---


AUTHOR
---
 - getAll -- 
 возвращает всех авторов -- 
 localhost:8000/author.php?action=getAll
 ---
 
 
 phpMyAdmin 
 ---
  - localhost:8001/ 
  - root : PASSWORD
 ---
